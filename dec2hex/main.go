// Script-friendly utility to convert decimal to hex.
package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	i, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println("ERR:INPUT-NOT-DEC")
	} else {
		fmt.Printf("%X\n", i)
	}
}
